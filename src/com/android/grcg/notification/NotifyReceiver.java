package com.android.grcg.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.android.grcg.R;

public class NotifyReceiver extends BroadcastReceiver{
	RemoteViews contentView = null;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		String title = intent.getStringExtra("Message");
		int id = 1;
		NotificationManager m_NotificationManager = (NotificationManager) context
				.getSystemService(context.NOTIFICATION_SERVICE);
		
		
		Notification m_Notification = new Notification(
				R.drawable.warning_notification_small, title,
				System.currentTimeMillis());
		
		contentView = new RemoteViews(context.getPackageName(), R.layout.notification);
		 m_Notification.defaults = Notification.DEFAULT_ALL;
		 
		 
		// LED灯闪烁 yjx
			m_Notification.defaults |= Notification.DEFAULT_LIGHTS;
			m_Notification.defaults |= Notification.DEFAULT_SOUND;
			
			m_Notification.flags = m_Notification.flags
					| Notification.FLAG_AUTO_CANCEL | Notification.FLAG_SHOW_LIGHTS;
			
			PendingIntent my_PendingIntent = PendingIntent.getActivity(context,
					0, new Intent(), 0);
			m_Notification.setLatestEventInfo(context, context.getResources().getText(R.string.app_name), title,
					my_PendingIntent);
			m_Notification.contentView = contentView;
	}

}
